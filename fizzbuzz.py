fizz_num = int(input('What is your first number?  '))
buzz_num = int(input('What is your second number?  '))
fizz = str(input('What is your first word?  '))
buzz = str(input('What is your second word?  '))

i = 0

for i in range(100):  # Could also use for i in range(100):
    i += 1
    if i % (fizz_num * buzz_num) == 0:
        print(fizz + buzz)
    elif i % buzz_num == 0:
        print(buzz)
    elif i % fizz_num == 0:
        print(fizz)
    else:
        print(i)
