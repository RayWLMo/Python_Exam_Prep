# # Q1 - declare a function called username that takes 1 argument as a string and return the name
#
# def username(name):
#     return name
#
# print(username("user_name"))

# # Q2 - declare a list with numbers 1 to 5. Iterate through the list and display through the list
#
# number_list = [1, 2, 3, 4, 5]
# for a in number_list:
#     print(a)
#
# # Q3 - and - && - & - == Create
# # Which one returns a boolean value
# name = "Ray"
# if name == "Ray":
#     print("Hi")

# Q4 - What is the difference between a list and a tuple
# A list is mutable and a tuple is immutable

# Q5 - Can we add an element to a list
# Can we add an element to a tuple
# Can the element of a tuple be different types
# Yes, No, Yes

# Q6 - Create a dict with key value pairs, first_name and last_name
# name_dict = {"first_name": "Raymond", "last_name": "Mo"}
# print(type(name_dict))

# # Q7 - Add course into the dict with a method
# name_dict = {"first_name": "Raymond", "last_name": "Mo"}
#
# name_dict["Course"] = "DevOps"

# Q8 - Create a class called student, initialise the class and create an object of the class

# class student:
#     def __init__(self):
#         pass
#
#
# DevOps_student = student()

""" Q9 - Create 2 functions that take 2 arguments each. function 1 called 'add_values', function 2 called
subtract_values. Return the addition and subtraction respectively """


# def add_values(a, b):
#     return a + b
#
#
# def subtract_values(a, b):
#     return a - b

# # Q10 - Declare a dict with 3 shopping items with cost, eggs is 1.20, milk is 2.30, bread is 1.00
# # Write a function that returns the total value
#
# shopping = {"eggs": 1.2, "milk": 2.3, "bread": 1.0}
#
# def total_value():
#     return shopping["eggs"] + shopping["milk"] + shopping["bread"]
#
# print(total_value())

# # Q11 - Prompt the user to enter an integer, declare a function that checks if the number is odd or even. Display back to the user whether or not the number is odd or even

# num = int(input("Please enter a number:  "))
#
# def odd_or_even(a):
#     if a % 2 == 0:
#         return "Your number is even"
#     else:
#         return "Your number is odd"
#
# print(odd_or_even(num))

# # Q12 - select the correct syntax - 1 -super.__init(). 2- super()__init(). 3 super().__init(). 4 - super().__init__()
# Super.__init__()

# class Jimbo:
#     def __init__(self):
#         super().__init__()

# # Q13 - Declare a tuple with three values and iterate though the tuple and display the value

# my_tuple = ("A", "b", 156)
# for i in my_tuple:
#     print(i)

# # Q14 - Create a class called Student, with 1 method called student_data that returns student_name
# # Create a class called DevOpsStudent which inherits the Student class that returns student_name

# class Student:
#     def Student_Data(self):
#         return "james"
#
#
# class DevOpsStudent(Student):
#     def __init__(self):
#         super().__init__()
#
# dev = DevOpsStudent()
# print(dev.Student_Data())

# # Q15 - Declare a variable called 'city' and declare a method that takes 'city' as an argument and value of 'city' is 'London' and method checks if value is London then return True

# city = 'London'
#
# def city_name(a):
#     if a == 'London':
#         return True
#     else:
#         return False
#
# print(city_name(city))

# Q16 - Import random. Create a function that takes 2 arguments and returns the percentage

import random

def percentage(a, b):
    return str((a * 100)/b) + "%"


ran_number1 = random.randint(0, 100)
ran_number2 = random.randint(0, 100)

print(percentage(ran_number1, ran_number2))
