# Variables
int_var = 1
float_var = 1.0
str_var = "abc"

# Operators

print(1 + 1)
print(2 ** 2)
print(3 * 3)
print(4 / 4)
print(5 % 5)
print(6 - 6)

# Comparison

print(2 == 2)
print(1 != 3)
print(5 <= 6)
print(7 >= 3)

# if statements
a = 4
b = 5
if a > b:
    print('Yes')
elif a < b:
    print('No')
else:
    print('Eh')

# while loops
i = 1
while i <= 10:
    print(i)
    i += 1

var = 'Hello World!'
print(var[6:11])

arb_list = [1, 2, 3, 'four', 'five']
arb_list.append('six')
arb_list.remove(1)  # for specific item
arb_list.pop(1)  # for index
arb_list[2] = 5
print(arb_list)

for i in arb_list:
    print(i)

arb_set = {1, "two", 3, 'four', 1}
arb_set.add('six')
arb_set.remove("two")
arb_set.discard('4')
# cannot change items, no duplicates
print(arb_set)

arb_tuple = (1, 'two', 3, 'four')
print(arb_tuple[2])

frozen_set = frozenset([1, 'two', "3", 4.0])
print(frozen_set)


def function_name(arg1, arg2):
    return arg1


class class_name:
    def __init__(self):
        self.attribute = True

        self._hidden_att = 'hidden var'

    def class_method(self, arg1):
        return arg1, self.attribute


class child_class(class_name):
    def __init__(self):
        super().__init__()

        class_name.__init__(self)
        print(self._hidden_att)
    def child_method(self, arg1):
        return arg1 * 5


object_name = child_class()
prompt = int(input('Enter Number:  '))
print(object_name.class_method(prompt))
print(object_name.child_method(prompt))

