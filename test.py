tlist = [1, '2', 3.0, 'four']
print(tlist[0])
tlist[0] = 'one'
tlist.remove('four')
tlist.pop(2)
print(tlist)  # ['one','2']

ttuple = ('one', 2, '3', 4.0)
# Tuples are immutable
print(ttuple[2])

tset = {1.0, 'two', 3, '4'}
# print(tset[2]) # Sets are unordered and un-indexed
# Sets are unchangeable but can add to set
tset.add('five')
print(tset)

tdict = {'num1': '1', 'num2': 2.0, 'num3': 'three', 'list': [1, 2, 4, 'four']}
print(tdict['num2'])  # Or print(tdict.get('num2'))
print(tdict.values())
print(tdict.keys())
print(tdict['list'][3][1])
tdict['num1'] = 1  # Updates the key with the new value
tdict['num5'] = 'five'  # Or if key doesn't exist, it will create a new one with the value
tdict['list'].remove(1)
tdict.pop('num2')
print(tdict)

